@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="/store" method="post">
                    {{ csrf_field() }}
                        <div class="form-group">
                                <label>Nama Lengkap</label>
                            <input type="text" class="form-control" name="nama" placeholder="">
                        </div>
                        <div class="form-group">
                            <label >Alamat KTP</label>
                            <input type="text" class="form-control" name="alamat" >
                        </div>
                        <div class="form-group">
                                <label>Pekerjaan</label>
                            <input type="text" class="form-control"  name="pekerjaan" placeholder="">
                        </div>
                        <div class="form-group">
                            <label >Pendidkan Terakhir</label>
                            <input type="text" class="form-control"  name="pendidikan">
                        </div>
                        <div class="form-group">
                                <label>Nomor Telepon</label>
                            <input type="number" class="form-control"  placeholder="" name="nomor">
                        </div>
                        <div class="form-group">
                                <label>Username</label>
                            <input type="text" class="form-control"  placeholder="" name="username">
                        </div>
                        
                        <button type="submit" class="btn btn-primary">Ubah Data</button>
                        </form>

                  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';

    protected $fillable = ['alamat,pekerjaan,nama,pendidkan,nomor'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
